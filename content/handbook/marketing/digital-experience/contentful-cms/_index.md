---

title: "Contentful CMS"
description: "Editing and creating content using Contentful"
---



# Contentful Guide

The Digital Experience team is currently migrating the [marketing site](https://about.gitlab.com/) to our new CMS tool, Contentful. This work is ongoing throughout FY24Q4 and into the new year, but we are starting to onboard different teams onto Contentful so that they may self-serve their own content changes. 

### What is Contentful?

Contentful is a Content Management System. It replaces all of the text files in our marketing site (such as events.yml) and replaces them with different Content Entries. Contentful also has a Media Library, where we are moving our images. 

### Contentful terminology

- Content: A Catch-all term for text or images on the marketing site
- Content Type: A type of content. These include Cards, Heros, Buttons, etc and can all be viewed in Contentful under the Content Model tab. We have different classes of Content Types (Atoms, Molecules, Organisms)
  - Atoms: Base-level components that don't contain other Content Types within them. Example: Buttons, Text
  - Molecules: Typically contain a combination of Atoms. Example: Hero, Card Group
  - Organisms: Commonly contain many Molecules or Atoms and are considered more complex. Example: Page
- Entry: An Entry is considered one piece of content in a content type. For example, there are many Cards across the marketing site, but the Free card on the pricing page is one example of a Card Entry.
- Field: Even the most basic Content Type typically has multiple fields. For example, a Button may seem small, but some fields it contains include the url it points to, the text on the button, perhaps an icon, maybe some tracking data. Each of these are considered Fields of the Button Content Type. 


## Resource Deep Dive 


1. [Training Resources for Contentful](/handbook/marketing/digital-experience/contentful-cms/editing-content)
2. [Custom Pages](/handbook/marketing/digital-experience/contentful-cms/custom-pages)
4. [Known knowns & feature wishlist](/handbook/marketing/digital-experience/contentful-cms/wishlist)


## Getting help with Contentful

### Contentful Office Hours

The Digital Experience team will be hosting weekly drop-in office hours for specific questions, walkthroughs, or feature requests. We can screenshare and work through issues together. The office hours are on the digital experience calendar and the marketing team calendar.

Times: 
- Every Tuesday from 11:30am EST - 11:55am EST
- Every second Thursday from 4:00pm EST - 4:55pm EST

### Digital Experience Slack Channel

For urgent requests, reach out to our team in #digital-experience-team on slack. 



## Getting access to Contentful

Please fill out an access request [here.](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/)





